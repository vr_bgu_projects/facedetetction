﻿using OpenCvSharp;
using System;
using System.IO;
using System.Windows.Forms;



namespace OpenCV
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Load the cascades
            var haarCascade = new CascadeClassifier(FilePath.Text.HaarCascade);
            var lbpCascade = new CascadeClassifier(FilePath.Text.LbpCascade);

            // Detect faces
            Mat haarResult = DetectFace(haarCascade,textBoxFilePath.Text);
            Mat lbpResult = DetectFace(lbpCascade, textBoxFilePath.Text);

            Cv2.ImShow("Faces by Haar", haarResult);
            Cv2.ImShow("Faces by LBP", lbpResult);
            Cv2.WaitKey(0);
            Cv2.DestroyAllWindows();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cascade"></param>
        /// <returns></returns>
        private Mat DetectFace(CascadeClassifier cascade, string filePath)
        {
            Mat result;

            var src = new Mat(filePath == null || filePath == "" ? FilePath.Image.Yalta : filePath, ImreadModes.Color);
                        
            using (var gray = new Mat())
            {
                result = src.Clone();
                Cv2.CvtColor(src, gray, ColorConversionCodes.BGR2GRAY);

                // Detect faces
                Rect[] faces = cascade.DetectMultiScale(
                    gray, 1.08, 2, HaarDetectionType.ScaleImage, new Size(30, 30));

                // Render all detected faces
                foreach (Rect face in faces)
                {
                    var center = new Point
                    {
                        X = (int)(face.X + face.Width * 0.5),
                        Y = (int)(face.Y + face.Height * 0.5)
                    };
                    var axes = new Size
                    {
                        Width = (int)(face.Width * 0.5),
                        Height = (int)(face.Height * 0.5)
                    };
                    Cv2.Ellipse(result, center, axes, 0, 0, 360, new Scalar(255, 0, 255), 4);
                }
            }
            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                textBoxFilePath.Text = file;
                pictureBox1.ImageLocation = file;
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

            }
        }
    }

}
